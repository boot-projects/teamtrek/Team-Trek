package com.teamTrek;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
//import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@SpringBootApplication
@EnableTransactionManagement
//@CrossOrigin(origins = {"http://localhost:4200"})
public class TeamTrekApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeamTrekApplication.class, args);
	}
	
//	@Bean
//	public CorsFilter corsFilter() {
//		CorsConfiguration corsConfiguration = new CorsConfiguration();
//		corsConfiguration.setAllowCredentials(true);
//		corsConfiguration.setAllowedOrigins(Arrays.asList("http://localhost:4200"));
//		corsConfiguration.setAllowedHeaders(Arrays.asList("Origin", "Access-Control-Allow-Origin", "Content-Type", 
//				"Accept", "Authorization", "Origin, Accept","X-Requested-With", "Access-Control-Request_method",
//				"Access-Control-Request-Headers"));
//		corsConfiguration.setExposedHeaders(Arrays.asList("Origin", "Access-Control-Allow-Origin", "Content-Type", 
//				"Accept", "Access-Control-Allow-Credentials"));
//	corsConfiguration.setAllowedMethods(Arrays.asList("Post","Put", "Get", "Delete", "Options"));
//	UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
//	urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
//	return new CorsFilter(urlBasedCorsConfigurationSource);
//	}
	
	 @Bean
	    public CorsFilter corsFilter() {
	        CorsConfiguration corsConfiguration = new CorsConfiguration();
	        corsConfiguration.setAllowCredentials(true);
	        corsConfiguration.setAllowedOrigins(Arrays.asList("http://localhost:4200"));
	        corsConfiguration.setAllowedHeaders(Arrays.asList(
	            "Origin", "Access-Control-Allow-Origin", "Content-Type",
	            "Accept", "Authorization", "X-Requested-With",
	            "Access-Control-Request-Method", "Access-Control-Request-Headers"
	        ));
	        corsConfiguration.setExposedHeaders(Arrays.asList(
	            "Origin", "Access-Control-Allow-Origin", "Content-Type",
	            "Accept", "Access-Control-Allow-Credentials"
	        ));
	        corsConfiguration.setAllowedMethods(Arrays.asList("POST", "PUT", "GET", "DELETE", "OPTIONS"));

	        UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
	        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);

	        return new CorsFilter(urlBasedCorsConfigurationSource);
	    }

}
