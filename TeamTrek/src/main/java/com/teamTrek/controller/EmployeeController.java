package com.teamTrek.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.teamTrek.model.Employee;
import com.teamTrek.service.EmployeeService;

import jakarta.transaction.Transactional;


@RestController
@RequestMapping("/employee")
public class EmployeeController {
//	@Autowired
	private final EmployeeService employeeService;
	
	
	public EmployeeController(EmployeeService EmployeeService) {
		this.employeeService =EmployeeService;
	}
	
	@GetMapping("/all")
	public ResponseEntity<List<Employee>> getEmployees(){
		var employees = this.employeeService.findAllEmployees();
		
		return new ResponseEntity<>(employees,HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") Integer id){
		var employee = this.employeeService.findEmployeeById(id);
		
//		if(employee instanceof Employee) {
//			return new ResponseEntity<>(employee, HttpStatus.FOUND);
//		}else {
//			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//		}
		
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?> addEmployee(@RequestBody Employee employee){
		var emp = this.employeeService.addEmployee(employee);
		
		if(emp instanceof Employee) {
//			
			return ResponseEntity.status(HttpStatus.CREATED).build();
		}else {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
//		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@PostMapping("/addAll")
	public ResponseEntity<?> addAllEmployee(@RequestBody List<Employee> employee){
		this.employeeService.addEmployeeOfList(employee);
		
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@PutMapping("/update")
	public ResponseEntity<?> updateEmployeeById( @RequestBody Employee employee){
		Integer id = employee.getId();
		var emp = this.employeeService.findEmployeeById(id);
		
		if(emp != null) {
			this.employeeService.updateEmployee(employee);
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@Transactional
	@DeleteMapping("/delete/{id}")
	
	public ResponseEntity<?> deleteEmployeeById(@PathVariable("id") Integer id){
		this.employeeService.deleteEmployeeById(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@Transactional
	@DeleteMapping("/deleteAll")
	
	public ResponseEntity<?> deleteAll(){
		this.employeeService.deleteAll();
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	
	

	
	
}
