package com.teamTrek.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.teamTrek.model.Employee;

public interface EmployeeRepo extends JpaRepository<Employee, Integer> {

	List<Employee> findAll();

	Optional<Employee> findEmployeeById(Integer id);

	void deleteEmployeeById(Integer id);
	
//	List<Employee> findAllEmployeesList(); no

}
