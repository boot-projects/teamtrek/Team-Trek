package com.teamTrek.service;

import java.util.List;
import java.util.UUID;

//import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.teamTrek.exception.UserNotFoundException;
import com.teamTrek.model.Employee;
import com.teamTrek.repo.EmployeeRepo;

@Service
public class EmployeeService {
	
	private final EmployeeRepo employeeRepo;
	
	public EmployeeService(EmployeeRepo employeeRepo) {
		this.employeeRepo=employeeRepo;
	}
	//adding a new Employee
	public Employee addEmployee(Employee employee){
		employee.setEmployeeCode(UUID.randomUUID().toString());
		return employeeRepo.save(employee);
	}
	
	//Getting all employees
	public List<Employee> findAllEmployees(){
		return employeeRepo.findAll();
	}
	
	//find employee by Id
	public Employee findEmployeeById(Integer id) {
		return employeeRepo.findEmployeeById(id)
				.orElseThrow(() -> new UserNotFoundException("User not found by id: "+ id));
	}
	
	//updating the employee
	public Employee updateEmployee(Employee employee) {
		return employeeRepo.save(employee);
	}
	
	public void deleteEmployeeById(Integer id) {
		 employeeRepo.deleteEmployeeById(id);
	}
	public void addEmployeeOfList(List<Employee> employees) {
		for(Employee emp : employees) {
			emp.setEmployeeCode(UUID.randomUUID().toString());
		}
		employeeRepo.saveAll(employees);
	}
	
	public void deleteAll() {
		this.employeeRepo.deleteAll();
	}
	
}
